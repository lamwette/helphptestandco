<?php
require_once 'vendor/autoload.php';

class PagesIntegrationTest extends IntegrationTest
{

    public function test_index()
    {
        $response = $this->make_request("GET", "/");
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals("Hello World!", $response->getBody()->getContents());
        $this->assertContains("text/html", $response->getHeader('Content-Type')[0]);
    }

    public function test_hello()
    {
        $response = $this->make_request("GET", "/hello/Jacquie");
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals("hello, Jacquie!", $response->getBody()->getContents());

    }

    public function test_sexe()
    {
        $response = $this->make_request("GET", "/sexe/pipou");
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals("je fais du sexe avec la maman de pipou!", $response->getBody()->getContents());

    }

    public function test_taille()
    {
        $response = $this->make_request("GET", "/taille/5");
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals("la taille du sexe de pipou est de 5 mm!", $response->getBody()->getContents());

    }
}
?>