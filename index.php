<?php
require "vendor/autoload.php";
$loader = new Twig_Loader_Filesystem(dirname(__FILE__) . '/views');

$twigConfig = array
(
    // 'cache' => './cache/twig/',
    // 'cache' => false,
    'debug' => true,
);


Flight::register('view', 'Twig_Environment', array($loader, $twigConfig), function ($twig) 
{
    $twig->addExtension(new Twig_Extension_Debug()); // Add the debug extension
    
    //Mon filtre twig transformant le html en markdown
    $twig->addFilter(new Twig_Filter('markdown', function($string)
    {
        return renderHTMLFromMarkdown($string);
    }));

});

//utilisation de ORM pour les bases de données
Flight::before('start', function(&$params, &$output){
    ORM::configure('sqlite:ingen.sqlite3');
});

//utiliser render au lieu de view()->display()
Flight::map('render', function($template, $data=array())
{
    Flight::view()->display($template, $data);
});
// la route de récup des livres
Flight::route('/books', function()
{
    $data = 
    [
        'books' => recover_books(),
    ];

    Flight::render('books.twig', $data);
});

//la route de récup des fichiers de markdown
Flight::route('/markdown_test', function()
{

    $test =
    [
       'test' => readPagesContent('test'),
    ]; 
    Flight::render('markdown_test.twig', $test);
});

//début de Flight
Flight::start();
?>