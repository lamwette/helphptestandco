<?php
function multiply($a, $b)
{
    return $a * $b;
}
//recupération des livres depuis l'api du prof
function recover_books()
{
    $response = Requests::get('https://allosaurus.delahayeyourself.info/api/books/');
    
    return json_decode($response->body);
}

use Michelf\Markdown;
// convertion du HTML en markdown
function renderHTMLFromMarkdown($string_markdown_formatted)
{
    return Markdown::defaultTransform($string_markdown_formatted);
}

//lire le contenu d'un fichier
function readFileContent($filepath)
{
    return file_get_contents($filepath);
}

//lire le contenu sans l'extension
function readPagesContent($pagename)
{
    $pagepath = sprintf("pages/%s.md", $pagename);
    return readFileContent($pagepath);
}
?>